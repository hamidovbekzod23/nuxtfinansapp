import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'
// import axios from 'axios'

Vue.use(Vuex)

const state = () => ({
    user: {} // Нужно постоянно пополнять
})

const getters = {
    user: (state) => state.user
}

const mutations = {
    SAVE_USER(state, newObj) {
        state.user = newObj
        localStorage.user = JSON.stringify(newObj)

        this.$router.push({ path: "/Register" })

    },
    GET_USER(state) {
        state.user = JSON.parse(localStorage.user)
    },
    REGISTER_USER(state, newObj){
        state.user = newObj
        localStorage.user = JSON.stringify(newObj)

        this.$router.push({ path: "/MainPage" })
    }
}

const actions = {
    
    LOGIN({ commit }) {
        let obj = {}
        let fm = new FormData(event.target)

        fm.forEach((value, key) => {
            obj[key] = value
        })

        axios.post('https://finance-app-wepro.herokuapp.com/auth/login', obj)
            .then(res => {
                commit('SAVE_USER', res.data)
            })
            .catch(err => {
                alert('ERROR')
            })
    },
    REGISTER({ commit }){
        let obj = {}
        let fm = new FormData(event.target)

        fm.forEach((value, key) => {
            obj[key] = value
        })

        axios.post('https://finance-app-wepro.herokuapp.com/auth/signup', obj)
            .then(res => {
                commit('REGISTER_USER', res.data)
            })
            .catch(err => {
                alert('ERROR')
            })
    }
}

export default {
	state,
	getters,
	actions,
	mutations,
}